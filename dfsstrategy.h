#ifndef DFSSTRATEGY_H
#define DFSSTRATEGY_H

#include<deque>
#include<forward_list>

#include"abstractstrategy.h"

/*!
 * \brief The DFSStrategy class implements Depth First Search algorythm
 */
class DFSStrategy : public AbstractStrategy
{
public:
    DFSStrategy();

    virtual void addStatesToQueue(std::vector<State*>&& states, unsigned int level, Node* parent = nullptr) override;

    virtual std::pair<Node*, unsigned int> getNextNode() override;

    virtual void reset() {
        m_WorkQueue.clear();
        m_Nodes.clear();
        m_NodesCounter = 0;
    }

protected:
    /*!
     * \brief m_WorkQueue holds pairs of Nodes and their levels awaiting for process
     */
    std::deque<std::pair<Node*, unsigned int>>    m_WorkQueue;

    /*!
     * \brief m_Nodes holds all Node objects primarily to easly destroy them after all processing
     */
     std::forward_list<Node> m_Nodes;
};

#endif // DFSSTRATEGY_H
