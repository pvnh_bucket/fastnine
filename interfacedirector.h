#ifndef INTERFACEDIRECTOR_H
#define INTERFACEDIRECTOR_H

#include <QObject>
#include <QStringList>
#include <QHash>
#include "solution.h"

class InterfaceDirector : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList states READ states WRITE setStates NOTIFY statesChanged)
    Q_PROPERTY(QString statusString READ statusString NOTIFY statusChanged)
    Q_PROPERTY(double dFSIDepth READ dFSIDepth WRITE setDFSIDepth NOTIFY dFSIDepthChanged)

public:
    explicit InterfaceDirector(QObject *parent = 0);
    virtual ~InterfaceDirector();

    enum StrategyType {
        DFS, DFSI, AStarManhattan, AStarWrongDigitsCount
    };

signals:
    void statesChanged();
    void statusChanged();
    void dFSIDepthChanged();
    void endSolutionSearch();
    void stopSearch();

public slots:

double dFSIDepth() const {
        return m_DFSIDepth;
    }

void setDFSIDepth(double depth) {
        if(depth!=m_DFSIDepth) {
            m_DFSIDepth = depth;
            emit dFSIDepthChanged();
        }
    }

QStringList states() const {
        return m_States;
    }

void setStates(const QStringList& states) {
        if(m_States != states) {
            m_States = states;
            emit statesChanged();
        }
    }

QString statusString() const {
    return m_StatusString;
}

void endSearch();

void newStatus();

void setupSolution(const QString& originState, const QString& targetState, const QString solutionType);

void findSolution();

private:
    QStringList     m_States;
    Solution*       mp_Solution;
    QString         m_StatusString;
    double          m_DFSIDepth;

    static QHash<QString, int> strategiesHash;

    static const int INITIAL_DFSI_DEPTH = 9;

    void getSolutionStatus();
};

#endif // INTERFACEDIRECTOR_H
