import QtQml.Models 2.1
import QtQuick 2.5

Rectangle {
    id: root
    width: 220
    height: 240
    property alias text: label.text

    function getStateString() {
        var i;
        var str = "";
        for (i = 0; i< visualModel.items.count; ++i) {
            str += visualModel.items.get(i).model.modelData;
        }
        return str;
    }

    Text {
        id: label
        width: 210
        height: 30
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        text: ""
    }

    GridView {
        id: mainGrid
        width: 210
        height: 210
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: label.bottom
        keyNavigationWraps: false
        cellHeight: 70
        cellWidth: 70

        interactive: false

        model: DelegateModel {
            id: visualModel
            delegate: stateDelegate
            model: ["1", "2", "3", "4", "5", "6", "7", "8", "0"]
        }

        Component {
            id: stateDelegate
            MouseArea{
                id: dragArea
                enabled: root.enabled
                width: 70
                height: 70
                drag.target: {if(modelData == "0") return undefined; else return icon} //icon

                property int visualIndex: DelegateModel.itemsIndex

                Rectangle {
                    id: icon
                    width: 60
                    height: 60
                    color: {if(modelData == "0") return "#ffffff"; else return "#909090" }

                    Drag.active: dragArea.drag.active
                    Drag.source: dragArea
                    Drag.hotSpot.x: width / 2
                    Drag.hotSpot.y: height / 2

                    anchors {
                        horizontalCenter: parent.horizontalCenter;
                        verticalCenter: parent.verticalCenter
                    }

                    states: [
                        State {
                            name: "dragging"
                            when: icon.Drag.active;
                            ParentChange {
                                target: icon
                                parent: mainGrid
                            }

                            AnchorChanges {
                                target: icon;
                                anchors.horizontalCenter: undefined;
                                anchors.verticalCenter: undefined
                            }
                        }
                    ]

                    Text {
                        x: 5
                        text: {if(modelData == "0") return ""; else return modelData }
                        anchors {
                            horizontalCenter: parent.horizontalCenter;
                            verticalCenter: parent.verticalCenter
                        }
                        font.bold: true
                    }
                }//Rectangle
                DropArea {
                    enabled: root.enabled
                    anchors { fill: parent; margins: 15 }
                    onEntered: {
                        if(drag.source.parent.parent !== mainGrid)
                            return;
                        var si = drag.source.visualIndex;
                        var di = dragArea.visualIndex;
                        if(si > di) {
                            visualModel.items.move(
                                        si,
                                        di  );
                            visualModel.items.move(
                                        di+1,
                                        si  );
                        } else if (si < di) {
                            visualModel.items.move(
                                        si,
                                        di  );
                            visualModel.items.move(
                                        di-1,
                                        si  );
                        }
                    }
                }//DropArea

            }//MouseArea

        }//Component
    }
}
