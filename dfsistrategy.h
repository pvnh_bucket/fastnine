#ifndef DFSISTRATEGY_H
#define DFSISTRATEGY_H

#include "dfsstrategy.h"

/*!
 * \brief The DFSIStrategy class implements Depth First Search algorythm with an iterative depth boundary
 */
class DFSIStrategy : public DFSStrategy
{
public:
    /*!
     * \brief DFSIStrategy Constructs the DFSIStrategy object
     * \param maxLevel maximum level to which depth boundary may iterate
     */
    explicit DFSIStrategy(unsigned int maxLevel);

    virtual bool canGoToLevel(unsigned int level  ) override {
        if(level <= m_CurrentLevel)
            return true;
        else
            return false;
    }

    virtual std::pair<Node*, unsigned int> getNextNode() override;

    virtual void reset() override;

private:

    /*!
     * \brief m_MaxLevel maximum level to which the depth boundary may iterate
     */
    unsigned int m_MaxLevel;

    /*!
     * \brief m_CurrentLevel current boundary level
     */
    unsigned int m_CurrentLevel;

    /*!
     * \brief m_NextLevelReset Flag to mark "soft" reset between level iterations
     * used to save statistics and the level counter in this case of reset
     */
    bool m_NextLevelReset;
};

#endif // DFSISTRATEGY_H
