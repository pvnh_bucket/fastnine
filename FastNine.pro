TEMPLATE = app

QT += qml quick
CONFIG += c++11

RC_ICONS = FastNine.ico

SOURCES += main.cpp \
    solution.cpp \
    dfsstrategy.cpp \
    dfsistrategy.cpp \
    solutionfinder.cpp \
    interfacedirector.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    state.h \
    node.h \
    solution.h \
    abstractstrategy.h \
    dfsstrategy.h \
    dfsistrategy.h \
    astarstrategy.h \
    solutionfinder.h \
    interfacedirector.h

