#include "solution.h"

#include<iostream>
#include<string>

#include<QDebug>

Solution::Solution(const State &originState, const State &targetState, std::unique_ptr<AbstractStrategy> strategy):
    m_OriginState{originState},
    m_TargetState{targetState},
    mp_FoundTarget{nullptr},
    mp_Strategy{std::move(strategy)},
    m_ControlLevelOfPassedStates{true} //level control of passed states by default
{
    init(originState, targetState);
}

void Solution::run()
{
    //reset currently found target if any
    mp_FoundTarget = nullptr;

    if(!mp_Strategy)
        return;

    setRunFlag();

    //main cycle
    while(m_RunFlag) {
        try {
            std::unique_lock<std::mutex> lock(m_StatusMutex);
            //get current Node and its level from strategy
            std::pair<Node*, unsigned int> currentPair {mp_Strategy->getNextNode()};
            lock.unlock();
            Node* currentNodeP = currentPair.first;
            unsigned int currentLevel = currentPair.second;

            //chek if the current node contains a target state
            if(*(currentNodeP->getState()) == m_TargetState) {
                // Bingo!
                // Save target node and exit
                mp_FoundTarget = currentNodeP;
                break;
            }
            //chek if we can go down with child nodes
            if(mp_Strategy->canGoToLevel(currentLevel+1)) {
                //open current node
                lock.lock();
                openNode(currentNodeP, currentLevel);
            }
        }
        catch (AbstractStrategy::EmptyQueueException& ) {
			//Processing queue is empty, solution was not found, return now
			break;
		}
    }
    clearRunFlag();
}

void Solution::reset(const State &originState, const State &targetState, std::unique_ptr<AbstractStrategy> strategy)
{
    m_States.clear();

    m_PassedStates.clear();

    mp_FoundTarget = nullptr;

    mp_Strategy = std::move(strategy);

    init(originState, targetState);
}

void Solution::reset()
{
    m_States.clear();

    m_PassedStates.clear();

    mp_FoundTarget = nullptr;

    if(mp_Strategy) {
        mp_Strategy->reset();
        init(m_OriginState, m_TargetState);
    }
}

std::vector<std::string> Solution::getStatus() const
{
    std::vector<std::string> strings;

    if(m_RunFlag) {
        strings.push_back("Поиск решения...");
    }
    else {
        if(mp_FoundTarget == nullptr)
            strings.push_back("Решение не найдено!");
        else
            strings.push_back("Решение найдено!");
    }

    std::unique_lock<std::mutex> lock(m_StatusMutex);
    strings.push_back("Пройдено состояний: " + std::to_string(m_PassedStates.size()));

    //memory complexity
    strings.push_back("Максимум одновременно созданных узлов (потребление памяти):"
                      + std::to_string(mp_Strategy->maxNodesUsage()));
    //calculation complexity
    strings.push_back("Всего создавалось узлов (вычислитеьлная сложность):"
                      + std::to_string(mp_Strategy->totalNodesCreated()));
    lock.unlock();

    return strings;
}

void Solution::openNode(Node *nodeP, unsigned int level )
{
    State* currentState = nodeP->getState();

	if (!m_ControlLevelOfPassedStates) {
        if (wasStatePassed(*currentState))
			//This state was allready passed!
			return;
	}
	else {
        unsigned int passedLevel = wasStatePassed(*currentState);
		if ((passedLevel <= level) && (passedLevel > 0))
			//This state was passed in a higher level
			return;
	}

    //check this Node State in the passed states hashtable
    stateCheckPassed(currentState, level);

    //All states that we can reach from current node
    std::vector<State> newStates;
    currentState->getNextStates(newStates);

    //Unpassed states that we will add to the processing queue
    std::vector<State*> statesToAdd;
    statesToAdd.reserve(newStates.size());

    for(State& st : newStates) {
        //check if this state was passed and create new unpassed State object if not
		std::pair<unsigned int/*level*/, State*> passedPair = stateCheckPassed(&st, 0);

		if (!m_ControlLevelOfPassedStates) {
			if (passedPair.first)
				//This state was already passed
				continue;
		}
		else {
			unsigned int passedLevel = passedPair.first;
			if ((passedLevel <= level + 1) && (passedLevel > 0))
				//This state was passed in a higher level
				continue;
		}

        //add new State object to the temporary vector
		State* checkedState = passedPair.second;
        statesToAdd.push_back(checkedState);
    }

    //add all new states from the temporary vector to the processing queue
    mp_Strategy->addStatesToQueue(std::move(statesToAdd), level+1, nodeP);
}

void Solution::init(const State &originState, const State &targetState)
{
    if(!mp_Strategy)
        return;

    if(!(m_OriginState == originState))
        m_OriginState = originState;

    if(!(m_TargetState == targetState))
        m_TargetState = targetState;

    //assign this as a Solution object to current strategy
    mp_Strategy->assignSolution(this);

    //create origin state object
    m_States.emplace_front(originState);

    //obtain the pointer to the State object
    State* st = &(m_States.front());

    //add root state to the processing queue
    mp_Strategy->addStatesToQueue(std::vector<State*> {st}, 1);

    //check the origin state in the passed states hash table
    stateCheckPassed(st, 0);

}

