#include "dfsistrategy.h"
#include "solution.h"

DFSIStrategy::DFSIStrategy(unsigned int maxLevel ):
    m_MaxLevel{maxLevel},
    m_CurrentLevel{1},
    m_NextLevelReset{false}
{
}

std::pair<Node *, unsigned int> DFSIStrategy::getNextNode()
{
    try {
        return DFSStrategy::getNextNode();
    }
    catch(AbstractStrategy::EmptyQueueException) {
        if(m_CurrentLevel == m_MaxLevel)
            throw; // we have reached the maximum level of iteration so exit

        //go deeper one level with "soft" reset
        m_CurrentLevel +=1;
        m_NextLevelReset = true;
        mp_Solution->reset();
    }

    //first node after soft reset with new boundary level
    return DFSStrategy::getNextNode();
}

void DFSIStrategy::reset()
{
    if(m_NextLevelReset) {
        //soft reset
        m_NextLevelReset = false;
    }
    else {
        //hard reset - clear all stats and counters
        m_CurrentLevel = 1;
        m_TotalNodesCreated = 0;
        m_MaxNodesUsage = 0;
    }
    //clear internal data structures
    m_WorkQueue.clear();
    m_Nodes.clear();
    m_NodesCounter = 0;
}
