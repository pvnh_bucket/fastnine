#ifndef ABSTRACTSTRATEGY
#define ABSTRACTSTRATEGY

#include<vector>

#include "node.h"

class Solution;

/*!
 * \brief The AbstractStrategy class interface to all strategy classes.
 * A concrete strategy class must organize processing queue of Node elements, their sorting and holding.
 */
class AbstractStrategy {

public:
    /*we do not want to copy-construct or copy-assign any AbstractStrategy subclasses by default*/
    AbstractStrategy(const AbstractStrategy&) =delete;
    AbstractStrategy& operator=(const AbstractStrategy&) =delete;

    /*!
     * \brief AbstractStrategy default constructor
     * initializes to zero all private counters and assignes nullptr to an associated Solution
     * it will be called automaticly in all subclasses by default
     */
    AbstractStrategy():m_NodesCounter{0},
        m_TotalNodesCreated{0},
        m_MaxNodesUsage{0},
        mp_Solution{nullptr} {}

    /*!
     * \brief ~AbstractStrategy virual destructor does nothing in the base class
     */
    virtual ~AbstractStrategy() {}

    /*!
     * \brief assignSolution assignes Solution object to this strategy. Must be called from Solution on init stage
     * \param solutionP pointer to the Solution object
     */
    void assignSolution(Solution* solutionP) { mp_Solution = solutionP; }

    /*!
     * \brief addStatesToQueue adds states to the processing queue according to the concrete strategy
     * \param states vector of State pointers to add (State objects itself are stored in Solution object)
     * \param level the level of Node objects to be created from states
     * \param parentP parent Node object of states
     */
    virtual void addStatesToQueue(std::vector<State*>&& states, unsigned int level, Node* parentP = nullptr) = 0;

    /*!
     * \brief getNextNode returns current node from the processing queue to be processed in solution
     * Returned by this method Node pointer must point to the correct Node element whith existing parents (if not root)
     * in order to find a path from the current Node to the root
     * \return pair of pointer to the Node element and Node element's level in a search tree,
     * throws EmptyQueueException if processing queue is empty
     */
    virtual std::pair<Node*, unsigned int> getNextNode() = 0 ;

    /*!
     * \brief reset clears all internal data and states
     */
    virtual void reset() =0;

    /*!
     * \brief canGoToLevel check from solution if it can continue with nodes on the level or it must not open that nodes
     * useful with level restriction strategies in other cases always true
     * \param level
     * \return
     */
    virtual bool canGoToLevel(unsigned int /*level*/) {
        return true;
    }

    /*!
     * \brief totalNodesCreated default getter of computational complexity counter
     * \return total number of node creations
     */
    virtual unsigned long long totalNodesCreated() {
        return m_TotalNodesCreated;
    }

    /*!
     * \brief maxNodesUsage default getter of memory complexity counter
     * \return max number of simultaneously allocated node objects
     */
    virtual unsigned long long maxNodesUsage() {
        return m_MaxNodesUsage;
    }

    /*!
     * \brief The EmptyQueueException class is thrown by getNextNode() when processing queue is empty
     */
    class EmptyQueueException {};


protected:
    /*!
     * \brief m_NodesCounter holds a number of currently created Node elements
     */
    unsigned long long m_NodesCounter;

    /*!
     * \brief m_TotalNodesCreated is a measure of computational complexity of the algorythm
     * it increases every time the new node object is created
     */
    unsigned long long m_TotalNodesCreated;

    /*!
     * \brief m_MaxNodesUsage is a measure of memory complexity of the algorythm
     * it holds the maximum count of simultaneously allocated node objects
     */
    unsigned long long m_MaxNodesUsage;

    /*!
     * \brief mp_Solution holds pointer to the assigned solution
     */
    Solution * mp_Solution;
};

#endif // ABSTRACTSTRATEGY

