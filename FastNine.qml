import QtQuick.Window 2.2
import QtQml.Models 2.1
import QtQuick.Controls 1.4
import QtQuick 2.3
import fastnine 1.0

Rectangle {
    id: root
    width: 450
    height: 650
    color: "#00000000"
    state: "Input"
    states: [
        State {
            name: "Input"

            PropertyChanges {
                target: busyIndicator
                running: false
            }
            PropertyChanges {
                target: originalStateView;
                enabled: true;
            }
            PropertyChanges {
                target: targetStateView;
                enabled: true;
            }
            PropertyChanges {
                target: findButton;
                visible: true;
            }
            PropertyChanges {
                target: stopButton;
                visible: false;
            }
            PropertyChanges {
                target: modeCombo;
                enabled: true;
            }
            PropertyChanges {
                target: depthValue;
                enabled: true;
            }
        },

        State {
            name: "Searching"

            PropertyChanges {
                target: busyIndicator
                running: true
            }
            PropertyChanges {
                target: originalStateView;
                enabled: false;
            }
            PropertyChanges {
                target: targetStateView;
                enabled: false;
            }
            PropertyChanges {
                target: findButton;
                visible: false;
            }
            PropertyChanges {
                target: stopButton;
                visible: true;
            }
            PropertyChanges {
                target: modeCombo;
                enabled: false;
            }
            PropertyChanges {
                target: depthValue;
                enabled: false;
            }

        }
    ]

    Rectangle {
        id: statesrec
        height: 240
        color: "#00000000"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {
            id: statessubrec
            x: 230
            width: 450
            height: 240
            color: "#00000000"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 0

            Row {
                spacing:10
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                StateView {
                    id: originalStateView
                    text: "Начальное положение"
                }

                StateView {
                    id: targetStateView
                    text: "Конечное положение"
                }

            }
        }
    }


    Rectangle {
        id: buttonsrec
        height: 30
        color: "#ffffff"
        anchors.top: statesrec.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        Rectangle {
            id: buttonssubrec
            x: -94
            y: 40
            width: 450
            height: 30
            color: "#ffffff"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Row {
                id: row1
                x: 0
                y: 3
                anchors.verticalCenter: parent.verticalCenter
                spacing:5
                states: [
                    State {
                        name: "NoParams"
                        PropertyChanges {
                            target: depthValue
                            visible: false;
                        }
                    },
                    State {
                        name: "DepthBorder"
                        PropertyChanges {
                            target: depthValue
                            visible: true;
                        }
                    }
                ]

                Text {
                    id: comboLabel
                    width: 100
                    text: "Режим поиска:"
                    anchors.verticalCenter: modeCombo.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                ComboBox{
                    id: modeCombo
                    width: 200
                    model: ["В глубину", "В глубину итеративный", "A* Манхэттен", "A* количество цифр не на местах"]
                    onCurrentTextChanged: {
                        if(currentText == "В глубину итеративный")
                            row1.state = "DepthBorder"
                        else
                            row1.state = "NoParams"
                    }

                    function getSolutionType() {
                        if(currentText === "В глубину")
                            return "DFS";
                        if(currentText === "В глубину итеративный")
                            return "DFSI";
                        if(currentText === "A* Манхэттен")
                            return "AStarManhattan";
                        if(currentText === "A* количество цифр не на местах")
                            return "AStarWrongDigitsCount";
                    }
                }

                SpinBox {
                    id: depthValue;
                    value: 7
                }

                Button {
                    id: findButton
                    text: "Найти"
                    onClicked: {
                        root.state = "Searching"
                        interfaceDirector.setupSolution(originalStateView.getStateString(), targetStateView.getStateString(),
                                         modeCombo.getSolutionType());
                        interfaceDirector.findSolution();
                        return;
                    }
                }
                Button {
                    id: stopButton
                    text: "Остановить"
                    onClicked: {
                        interfaceDirector.stopSearch();
                        return;
                    }
                    visible: false
                }
            }
        }
    }

    //Row1

    InterfaceDirector {
        id: interfaceDirector
        //dFSIDepth: depthValue.value

        Component.onCompleted: {
            depthValue.value = dFSIDepth;
            dFSIDepth = Qt.binding(function() { return depthValue.value })
        }

        onEndSolutionSearch: {
            root.state = "Input"
        }

    }

    ScrollView {
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: buttonsrec.bottom
        anchors.topMargin: 10
        anchors.bottom: statusRec.top
        anchors.bottomMargin: 10

        BusyIndicator {
            id: busyIndicator
            width: 100
            height: 100
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            running: false
        }

        ListView {
            id: statesListView
            anchors.fill: parent
            boundsBehavior: Flickable.DragAndOvershootBounds
            clip: true
            keyNavigationWraps: false
            spacing: 15
            model: interfaceDirector.states
            delegate: statesListDelegate
        }
    }

    Component {
        id: statesListDelegate

        Row {
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter

            Grid {
                id: stateGrid
                property string stateSting: modelData
                rows: 3
                columns: 3
                spacing: 2

                Repeater {
                    model: 9

                    Rectangle {
                        color: {
                            if(stateGrid.stateSting[index] === "0")
                                return "#ffffff";
                            else
                                return "#909090"
                        }
                        width: 40
                        height: 40
                        Text {
                            text: {
                                if(stateGrid.stateSting[index] === "0")
                                    return "";
                                else
                                    return stateGrid.stateSting[index];
                            }
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                            horizontalAlignment: Text.AlignHCenter
                            anchors.fill: parent
                        }
                    }
                }
            }
            Text {
                width: 120
                height: 20
                text: "Шаг: " + index
            }
        }
    }


    Rectangle {
        id: statusRec
        height: 100
        color: "#909090"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        TextArea {
            id: statusText
            anchors.fill: parent
            text: interfaceDirector.statusString
        }
    }
}



