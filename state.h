#ifndef STATE_H
#define STATE_H

#include <cstdlib> //abs()
#include <vector>
#include <memory>

/*!
 * \brief The State class represents one state of the 9-puzzle by one 32bit unsigned integer value
 * Class uses indexes in its methods to access digit values at specific puzzle cells
 * These is how indexes correspond to puzzle cells:
 * 8 7 6
 * 5 4 3
 * 2 1 0
 * Index with value 0 has the lower right cell.
 */
class State
{

public:

    /*!
     * \brief State constructs a state from unsigned int value
     * \param state_value Value to construct from
     */
    explicit State(unsigned int value):
        m_Value{value}
    {}

    /*!
     * \brief getValue Read only access to the 32 bit unsigned value representing this puzzle state
     * \return State's value
     */
    unsigned int getValue() const {
        return m_Value;
    }

    /*!
     * \brief operator == Compares this puzzle state with the other
     * \param other Other puzzle state to compare with
     * \return true if all digits of this puzzle state are at the same places
     * as in the other state and false if not
     */
    bool operator==(const State& other) const {
        return this->m_Value == other.m_Value;
    }

    /*!
     * \brief operator [] gives readonly access to puzzle digits by index (0-8)
     * \param index Index of digit in puzzle (0-8)
     * \return digit at specified index
     */
    unsigned int operator[] (unsigned int index) const {
        if(index > (STATE_ARRAY_SIZE - 1) )
            return 0;
        unsigned int shiftBase = State::pow(static_cast<int>(10), index);
        unsigned int tmpValue{m_Value};
        tmpValue/=shiftBase;

        return tmpValue % 10;
    }

    /*!
     * \brief swap Swaps two digits in current puzzle state
     * \param firstIndex index of first digit to swap (0-8)
     * \param secondIndex index of second digit to swap (0-8)
     * \return reference to this object
     */
    State& swapDigits (unsigned int firstIndex,unsigned int secondIndex  ) {
        if(firstIndex==secondIndex)
            return *this;

        std::unique_ptr<unsigned int[]> array = this->getArray();
        unsigned int tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
        this->setFromArray(array.get());

        return *this;
    }

    /*!
     * \brief getNextStates finds possible next states from the current state of a puzzle
     * \param vector will be filled by found possible State objects
     */
    void getNextStates(std::vector<State>&  vector) const {
        unsigned int zeroPosition = getZeroPosition();

        switch(zeroPosition) {
        case 0: vector.push_back(State(*this).swapDigits(0,1));
                vector.push_back(State(*this).swapDigits(0,3));
                break;
        case 1: vector.push_back(State(*this).swapDigits(1,0));
                vector.push_back(State(*this).swapDigits(1,4));
                vector.push_back(State(*this).swapDigits(1,2));
                break;
        case 2: vector.push_back(State(*this).swapDigits(2,5));
                vector.push_back(State(*this).swapDigits(2,1));
                break;
        case 3: vector.push_back(State(*this).swapDigits(3,0));
                vector.push_back(State(*this).swapDigits(3,4));
                vector.push_back(State(*this).swapDigits(3,6));
                break;
        case 4: vector.push_back(State(*this).swapDigits(4,1));
                vector.push_back(State(*this).swapDigits(4,5));
                vector.push_back(State(*this).swapDigits(4,7));
                vector.push_back(State(*this).swapDigits(4,3));
                break;
        case 5: vector.push_back(State(*this).swapDigits(5,2));
                vector.push_back(State(*this).swapDigits(5,8));
                vector.push_back(State(*this).swapDigits(5,4));
                break;
        case 6: vector.push_back(State(*this).swapDigits(6,3));
                vector.push_back(State(*this).swapDigits(6,7));
                break;
        case 7: vector.push_back(State(*this).swapDigits(7,4));
                vector.push_back(State(*this).swapDigits(7,8));
                vector.push_back(State(*this).swapDigits(7,6));
                break;
        case 8: vector.push_back(State(*this).swapDigits(8,5));
                vector.push_back(State(*this).swapDigits(8,7));
                break;
        }
    }

    /*!
     * \brief manhattanDistance counts a manhattan distance from each digit in the curent state
     * to the same digit in the other state and summarizes them
     * \param other The other state
     * \return calculated sum
     */
    unsigned int manhattanDistance(const State& other) const {

        unsigned int resultDistance{0};

        std::unique_ptr<unsigned int[]> otherArray = other.getArray();
        std::unique_ptr<unsigned int[]> thisArray = this->getArray();

        std::vector<std::pair<int, int>> otherCoords(STATE_ARRAY_SIZE);

        //find x,y coordinates of each digit in the other state
        for(int i=0; i < STATE_ARRAY_SIZE; ++i) {
            //position in otherCoords vector is equal to the digit's value
            otherCoords[otherArray[i]] =
                    std::make_pair(i % 3, i / 3);
        }

        //for each digit in this state calculate manhattan dist to
        //the same digit in the other state and sum them
        for(int i=0; i < STATE_ARRAY_SIZE; ++i) {
            if(thisArray[i] != 0) {
                int currentX = i % 3;
                int currentY = i / 3;
                resultDistance += std::abs(currentX - otherCoords[thisArray[i]].first) +
                        std::abs(currentY - otherCoords[thisArray[i]].second);
            }
        }

        return resultDistance;
    }

    /*!
     * \brief wrongDigitsCount counts a number of digits in a puzzle which are not at the same
     * places as in the other state
     * \param other The other state
     * \return counted result
     */
    unsigned int wrongDigitsCount(const State& other) const {

        unsigned int result{0};

        std::unique_ptr<unsigned int[]> otherArray = other.getArray();
        std::unique_ptr<unsigned int[]> thisArray = this->getArray();

        //count the number of wrong placed digits
        for(int i=0; i < STATE_ARRAY_SIZE; ++i) {
            if(thisArray[i] != otherArray[i])
                result++;
        }

        return result;
    }

private:

    static const int STATE_ARRAY_SIZE = 9;

    unsigned int m_Value;

    std::unique_ptr<unsigned int[]> getArray() const {

        std::unique_ptr<unsigned int[]> arrayUP (new unsigned int[STATE_ARRAY_SIZE]);

        unsigned int tmpValue{m_Value};

        for(int i=0; i < STATE_ARRAY_SIZE; ++i) {
            arrayUP[i] = tmpValue % 10;
            tmpValue/=10;
        }

        return arrayUP;
    }


    /*!
     * \brief setFromArray sets internal value of this State object from an array
     * of unsingned int values. Since this method is for private use only we do not
     * check for array boundaries and nullptr for better performance
     * \param array Pointer to array of unsigned int values with STATE_ARRAY_SIZE size
     */
    void setFromArray(const unsigned int * array) {
        m_Value = 0;
        for(int i=(STATE_ARRAY_SIZE - 1); i>0; --i) {
            m_Value += array[i];
            m_Value *= 10;
        }
        m_Value += array[0];
    }

    unsigned int getZeroPosition() const {
        std::unique_ptr<unsigned int[]> array = this->getArray();
        unsigned int zeroPosition;
        for(zeroPosition=0; zeroPosition < STATE_ARRAY_SIZE; ++zeroPosition) {
            if(array[zeroPosition] == 0) break;
        }

        return zeroPosition;
    }

    /*!
     * \brief pow Computes the fixed point value of base raised to the power exp
     * \param base
     * \param exp
     * \return base^exp
     */
    static unsigned int pow(unsigned int base,unsigned int exp  ) {
        if(exp == 0)
            return 1;
        unsigned int result{base};
        for(unsigned int i=1; i < exp; ++i) {
            result *= base;
        }

        return result;
    }

};

#endif // STATE_H
