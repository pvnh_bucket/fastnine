#ifndef SOLUTIONFINDER_H
#define SOLUTIONFINDER_H

#include <QObject>
#include <QDebug>

#include "solution.h"

class SolutionFinder : public QObject
{
    Q_OBJECT
public:

    SolutionFinder(Solution *solutionP, QObject *parent = 0);

signals:
    void newStatus();
    void endSearch();

public slots:
    void startSearch();

    void stopSearch() {
        if(mp_Solution != nullptr)
            mp_Solution->clearRunFlag();
    }

private:
    Solution* mp_Solution;
};

#endif // SOLUTIONFINDER_H
