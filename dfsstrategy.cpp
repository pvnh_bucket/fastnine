#include "dfsstrategy.h"

DFSStrategy::DFSStrategy()
{
}

void DFSStrategy::addStatesToQueue(std::vector<State *> &&states, unsigned int level, Node *parent)
{
    //delete all nodes from previous brunch if any
    while(!m_Nodes.empty() && &(m_Nodes.front()) != parent) {
        m_Nodes.pop_front();
        //current memory usage counter
        --m_NodesCounter;
    }

    for(State* st : states) {
        //create new node object
        m_Nodes.emplace_front(st, parent);
        //current memory usage counter
        ++m_NodesCounter;
        //current calculation complexity counter
        ++m_TotalNodesCreated;
        //add that node to the beginning of the processing queue
        m_WorkQueue.emplace_front(&(m_Nodes.front()), level);
    }

    //maximum memory usage counting
    if(m_NodesCounter > m_MaxNodesUsage)
        m_MaxNodesUsage = m_NodesCounter;
}

std::pair<Node *, unsigned int> DFSStrategy::getNextNode()
{
    //throw exception if queue is empty
    if(m_WorkQueue.empty())
        throw EmptyQueueException();

    //remove first node frome queue and return it
    std::pair<Node *, unsigned int> retValue { m_WorkQueue.front() };
    m_WorkQueue.pop_front();
    return retValue;
}


