#ifndef SOLUTION_H
#define SOLUTION_H

#include<unordered_map>
#include<deque>
#include<forward_list>
#include<memory>
#include<atomic>
#include<mutex>

#include "node.h"
#include "abstractstrategy.h"

/*!
 * \brief The Solution class controls the process of finding puzzle solution.
 * It gets state objects (origin and target) and a strategy object which defines the search algorythm.
 * In the main method (run) solution object cycles infinite loop getting next Node from strategy object
 * checking if it is the target state (in this case execution stops) opening this Node (with checks
 * preventing the search path to cycle) and passing new State objects to the Strategy.
 */
class Solution
{
public:
    /*we do not want to copy-construct or copy-assign Solution objects*/
    Solution(const Solution&) =delete;
    Solution& operator=(const Solution&) =delete;

    /*!
     * \brief Solution Constructs Solution object
     * \param originState Original State object
     * \param targetState Target State object
     * \param strategy strategy defining object derived from AbstractStrategy
     */
    Solution(const State& originState, const State& targetState, std::unique_ptr<AbstractStrategy> strategy);

    /*!
     * \brief ~Solution empty virtual destructor. All member data will be deleted by their destructors
     */
    virtual ~Solution() {}

    /*!
     * \brief run Starts execution of main search cycle
     */
    void run();

    /*!
     * \brief getFound Returns the found solution
     * \return Pointer to a Node object which state is equal to target if search was successfull
     * and nullptr if wasn't
     */
    Node* getFound() {
        return mp_FoundTarget;
    }

    /*!
     * \brief reset Resets internal Solution state clearing all data.
     * Solution object becomes at state as it was right after construction but with provided initial parametars
     * \param originState Original State object
     * \param targetState Target State object
     * \param strategy strategy defining object derived from AbstractStrategy
     */
    void reset(const State& originState, const State& targetState, std::unique_ptr<AbstractStrategy> strategy);

    /*!
     * \brief reset Resets solution object with its initial parameters, also calls strategy.reset()
     */
    void reset();

    /*!
     * \brief getStatus Returns status information
     * \return vector of strings containing status info
     */
	 std::vector<std::string> getStatus() const;

    /*!
     * \brief targetState Provides read only access to target state object
     * \return target state object const reference
     */
    const State& targetState() const {
        return m_TargetState;
    }

    /*!
     * \brief setControlOfPassedStates Sets the policy of passed states level control
     * \param flag if false, new Nodes with states once passed during the search
     * process will not be added to the processing queue. If true, new Node with passed state will be
     * added if its level is lower than the level of Node allready passed this state
     */
    void setControlOfPassedStates(bool flag  ) {
        m_ControlLevelOfPassedStates = flag;
    }

    /*!
     * \brief setRunFlag sets the RunFlag. The search cycle will continue while this flag is set
     */
    void setRunFlag() {
        m_RunFlag = true;
    }

    /*!
     * \brief clearRunFlag clears the RunFlag. Do this if you want to stop search
     */
    void clearRunFlag() {
        m_RunFlag = false;
    }


private:

    /*!
     * \brief m_States holds all passed State objects
     */
    std::forward_list<State> m_States;

    /*!
     * \brief m_PassedStates hashtable of passed states to fast check possible cycles in a search proccess
     * Hashtable holds pointers to the State objects and levels they were passed at as values and State objects values as keys
     */
    std::unordered_map<unsigned int/*value*/, std::pair<unsigned int/*level*/, State*>>  m_PassedStates;

    /*!
     * \brief m_OriginState state to start search from
     */
    State  m_OriginState;

    /*!
     * \brief m_TargetState state to stop search at
     */
    State  m_TargetState;

    /*!
     * \brief mp_FoundTarget pointer to the Node with target status if search was successful
     */
    Node* mp_FoundTarget;

    /*!
     * \brief mp_Strategy Strategy object defines the search algorythm
     */
     std::unique_ptr<AbstractStrategy> mp_Strategy;

    /*!
     * \brief m_ControlLevelOfPassedStates if false, new Nodes with states once passed during the search
     * process will not be added to the processing queue. If true, new Node with passed state will be
     * added if its level is lower than the level of Node allready passed this state
     */
    bool m_ControlLevelOfPassedStates;

    /*!
     * \brief m_RunFlag clear this flag to stop search
     */
    std::atomic_bool m_RunFlag;

    /*!
     * \brief m_StatusMutex protects working data while getting status info from another thread
     */
    mutable std::mutex m_StatusMutex;

private:

    /*!
     * \brief init Private method used in construction and reset methods to fill the Solution
     * and strategy internal data structures with initial data
     * \param originState Original State object
     * \param targetState Target State object
     */
    void init(const State& originState, const State& targetState);

    /*!
     * \brief openNode Finds all possible states from given node, checks if this states were already passed
     * and passes this states to the strategy object
     * \param nodeP Node from which new states must be obtained
     * \param level Level of the given Node
     */
    void openNode(Node* nodeP,unsigned int level);

    /*!
     * \brief stateCheckPassed Checks the given state as passed with the given level
	 * if the given level is zero and there is a passed state with a nonzero level
	 * the level of the passed state will not be changed
     * \param stateP
     * \param level
	 * \return pair of a given state level and a pointer to it in a states list
     */
    std::pair<unsigned int/*level*/, State*> stateCheckPassed(State* stateP, unsigned int level= 0) {
        try {
            //if this state was passed update its level
            std::pair<unsigned int/*level*/, State*> &passedPair = m_PassedStates.at(stateP->getValue()); //this may throw
			//update level only if the given is nonzero
			if (level)
				passedPair.first = level;
			return passedPair;
        }
        catch (...) {
			//There is no such level in states list and hash, creating new
            m_States.emplace_front(*stateP);
            State* checkedState = &(m_States.front());
            //create new passed pair in hash
            m_PassedStates.emplace(checkedState->getValue(), std::make_pair(level, checkedState));
			return std::make_pair(level, checkedState);
        }
    }

    /*!
     * \brief wasStatePassed returns the level of a passed state and zero if the state was not passed
     * \param state State to check
     * \return level of a passed state and zero if the state was not passed
     */
    unsigned int wasStatePassed(const State& state) const {
        try {
            std::pair<unsigned int/*level*/, State*> const &passedPair = m_PassedStates.at(state.getValue());
            return passedPair.first;
        }
        catch (...) {
            return 0;
        }
    }

};

#endif // SOLUTION_H
