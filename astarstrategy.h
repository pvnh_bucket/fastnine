#ifndef ASTARSTRATEGY_H
#define ASTARSTRATEGY_H

#include<forward_list>
#include<queue>

#include"abstractstrategy.h"

/*!
 * \brief The PairComparator class compares recursive std::pair objects only by their first values
 * used in AStarStrategy class to organize a priority queue of processing nodes
 */
class PairComparator {
public:
    bool operator() (const std::pair<unsigned int, std::pair<Node*, unsigned int>>& left,
                     const std::pair<unsigned int, std::pair<Node*, unsigned int>>& right) {
        return left.first > right.first;
    }
};

/*!
 * \brief The ManhattanDistance class Used to parametrize AStarStrategy with h(x) calculated
 * from Manhattan distances between states
 */
class ManhattanDistance {
public:
    unsigned int operator() (const State& first, const State& second) const {
        return first.manhattanDistance(second);
    }
};

/*!
 * \brief The WrongDigitsCount class is used to parametrize AStarStrategy with h(x) calculated
 * from the number of digits not in the same cells as in the target state
 */
class WrongDigitsCount {
public:
    unsigned int operator() (const State& first, const State& second) const {
        return first.wrongDigitsCount(second);
    }
};


template <class HFunctionType>
/*!
 * \brief The AStarStrategy class implements A* search strategy. It must be parametrized with
 * a type of object implementing h(x) function. Such an object is assumed to have the following method
 * unsigned int operator() (const State& first, const State& second)
 */
class AStarStrategy : public AbstractStrategy
{
public:
    explicit AStarStrategy(const HFunctionType& hf = HFunctionType()):
        m_HFunction(hf) {}

    typedef std::pair<Node*, unsigned int> NodePair;
    typedef std::pair<unsigned int, NodePair> QueuedNodePair;
    typedef std::priority_queue <QueuedNodePair, std::vector<QueuedNodePair>, PairComparator> QueueType;

    virtual void addStatesToQueue(std::vector<State*>&& states, unsigned int level, Node* parent = nullptr) override {
        for(State* st : states) {
            //create a new node object
            m_Nodes.emplace_front(st, parent);
            ++m_NodesCounter;
            ++m_TotalNodesCreated;
            //add that node to the processing queue
            const State& target{mp_Solution->targetState()};
            unsigned int hDistance = m_HFunction((*st), target);
            m_WorkQueue.emplace(level/*g(x)*/ + hDistance/*h(x)*/, std::make_pair(&(m_Nodes.front()), level));
        }

        if(m_NodesCounter > m_MaxNodesUsage)
            m_MaxNodesUsage = m_NodesCounter;
    }

    virtual std::pair<Node*, unsigned int> getNextNode() override {
        //throw exception if queue is empty
        if(m_WorkQueue.empty())
            throw EmptyQueueException();

        //remove first node frome queue and return it
        NodePair retValue { m_WorkQueue.top().second };
        m_WorkQueue.pop();

        return retValue;
    }

    virtual void reset() {
        m_WorkQueue = QueueType{};
        m_Nodes.clear();
        m_NodesCounter = 0;
    }

private:
    /*!
     * \brief m_WorkQueue holds pairs of Nodes and their levels in turn wrapped in std::pair with their g(x)+h(x) values awaiting for process
     */
    QueueType m_WorkQueue;

    /*!
     * \brief m_Nodes holds all Node objects primarily to easly destroy them after all processing
     */
     std::forward_list<Node> m_Nodes;

    /*!
     * \brief m_HFunction functor calculating h(x) to the given node
     */
    HFunctionType m_HFunction;
};

#endif // ASTARSTRATEGY_H
