#include "solutionfinder.h"
#include <future>
#include <QThread>
#include <QEventLoop>
#include <QAbstractEventDispatcher>


SolutionFinder::SolutionFinder(Solution *solutionP, QObject *parent): QObject(parent),
    mp_Solution{solutionP}
{

}

void SolutionFinder::startSearch()
{
    if(mp_Solution == nullptr)
        return;

    std::future<void> future = std::async(std::launch::async, &Solution::run, mp_Solution);
    std::future_status status;
    do {
        status = future.wait_for(std::chrono::seconds(1));
        //make status update every one second
        emit newStatus();
        //process signals
        this->thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
    } while (status != std::future_status::ready);

    emit endSearch();
}
