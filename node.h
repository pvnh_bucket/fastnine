#ifndef NODE_H
#define NODE_H

#include "state.h"

/*!
 * \brief The Node class represents a node in a path search tree.
 * It has a pointer to the parent Node and to the State object which
 * describes a current puzzle state
 */
class Node
{
public:

    /*!
     * \brief Node Constructs a Node object
     * \param state pointer to the State object of current Node
     * \param parent pointer to the parent Node of current
     */
    Node(State* state, Node* parent) :
        mp_State{state},
        mp_Parent{parent}
    {}

    /*!
     * \brief getState provides access to the State object of current Node
     * \return pointer to the State object of current Node
     */
    State* getState() const {
        return mp_State;
    }

    /*!
     * \brief getParent provides access to the parent Node of current
     * \return pointer to the parent Node of current (nullptr if root)
     */
    Node* getParent() const {
        return mp_Parent;
    }

private:
    State*   mp_State;
    Node*   mp_Parent;
};

#endif // NODE_H
