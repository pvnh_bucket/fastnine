#include "interfacedirector.h"
#include "dfsstrategy.h"
#include "dfsistrategy.h"
#include "astarstrategy.h"
#include "solutionfinder.h"
#include <memory>
#include <utility>
#include <QThread>

InterfaceDirector::InterfaceDirector(QObject *parent) : QObject(parent),
    mp_Solution{nullptr},
    m_DFSIDepth{INITIAL_DFSI_DEPTH}
{}


InterfaceDirector::~InterfaceDirector()
{
    if(mp_Solution != nullptr)
        delete mp_Solution;
}

void InterfaceDirector::endSearch()
{
    Node* nodeP = mp_Solution->getFound();
    while(nodeP != nullptr) {
        QString stateString = QString::number(nodeP->getState()->getValue());
        //first zero in a numeric value will disappear so we need to add it in this case
        if(stateString.length() < 9)
            stateString.prepend("0");
        m_States.prepend(stateString);
        nodeP = nodeP->getParent();
    }

    emit statesChanged();
    getSolutionStatus();
    emit endSolutionSearch();
}

void InterfaceDirector::newStatus()
{
    getSolutionStatus();

}

QHash<QString, int> InterfaceDirector::strategiesHash {
    {"DFS", StrategyType::DFS},
    {"DFSI", StrategyType::DFSI},
    {"AStarManhattan", StrategyType::AStarManhattan},
    {"AStarWrongDigitsCount", StrategyType::AStarWrongDigitsCount}
};

void InterfaceDirector::setupSolution(const QString &originState, const QString &targetState, const QString solutionType)
{
    std::unique_ptr<AbstractStrategy> strategyUP;
    switch (strategiesHash[solutionType]) {
    case DFS:
        strategyUP.reset(new DFSStrategy);
        break;
    case DFSI:
        strategyUP.reset(new DFSIStrategy(static_cast<unsigned int>(m_DFSIDepth)));
        break;
    case AStarManhattan:
        strategyUP.reset(new AStarStrategy<ManhattanDistance>);
        break;
    case AStarWrongDigitsCount:
        strategyUP.reset(new AStarStrategy<WrongDigitsCount>);
        break;
    default:
        return;
    }
    if(mp_Solution == nullptr) {
        mp_Solution = new Solution(State(originState.toUInt()), State(targetState.toUInt()),
                                   std::move(strategyUP));
    }
    else mp_Solution->reset(State(originState.toUInt()), State(targetState.toUInt()),
                            std::move(strategyUP));

    //reset all status strings
    this->setStates(QStringList{});

    m_StatusString = QString();

    emit statusChanged();
}

void InterfaceDirector::findSolution()
{
    this->setStates(QStringList{});

    m_StatusString = QString();
    emit statusChanged();
    if(mp_Solution == nullptr)
        return;

    QThread* solutionThread = new QThread;
    SolutionFinder* solutionFinder = new SolutionFinder(mp_Solution);
    solutionFinder->moveToThread(solutionThread);

    connect(solutionThread, SIGNAL(started()), solutionFinder, SLOT(startSearch()));
    connect(solutionFinder, SIGNAL(newStatus()), this, SLOT(newStatus()));
    connect(solutionFinder, SIGNAL(endSearch()), this, SLOT(endSearch()));
    connect(this, SIGNAL(stopSearch()), solutionFinder, SLOT(stopSearch()));
    connect(solutionFinder, SIGNAL(endSearch()), solutionThread, SLOT(quit()));
    connect(solutionFinder, SIGNAL(endSearch()), solutionFinder, SLOT(deleteLater()));
    connect(solutionThread, SIGNAL(finished()), solutionThread, SLOT(deleteLater()));
    solutionThread->start();
}

void InterfaceDirector::getSolutionStatus()
{
    m_StatusString = QString();
    if(mp_Solution == nullptr)
        return;
    auto statusStrings = mp_Solution->getStatus();
    for(const std::string& str : statusStrings) {
        m_StatusString += QString::fromStdString(str);
        m_StatusString += QString("\n");
    }
    emit statusChanged();
}

