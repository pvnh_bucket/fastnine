#include <QGuiApplication>
#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

#include<iostream>

#include "solution.h"

#include "interfacedirector.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    qmlRegisterType<InterfaceDirector>("fastnine", 1, 0, "InterfaceDirector");

    view.setSource(QUrl("qrc:/FastNine.qml"));
    view.show();

    return app.exec();
}

